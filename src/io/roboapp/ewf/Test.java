package io.roboapp.ewf;


import io.roboapp.ewf.helper.Db;
import io.roboapp.ewf.util.Properties;

public class Test {
    public static void main(String[] args) throws Exception {

        String sDriverForClass = "org.sqlite.JDBC";
        String sUrlString = "jdbc:sqlite:data.sqlite";
        Db db = new Db(sDriverForClass, sUrlString);

        Properties dp = new Properties();
        dp.connect(db.getConnection());

        dp.setProperty("test-property-2", "second global property");
        dp.store();

        System.out.println(dp.getProperty("test-property"));
        System.out.println(dp.getProperty("test-property-2"));

        Properties p = new Properties(dp);
        p.connect(db.getConnection());
        p.link("default");
        System.out.println(p.getProperty("test-property"));
        System.out.println(p.getProperty("test-property-2"));

        p.setProperty("test-property-2", "Second item property");
        System.out.println(p.getProperty("test-property-2"));
        p.store();
    }
}
