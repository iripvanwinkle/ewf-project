package io.roboapp.ewf.components.resources.media;

import io.roboapp.ewf.Application;
import io.roboapp.ewf.Filter;
import io.roboapp.ewf.components.library.LibraryArea;
import io.roboapp.ewf.components.library.LibraryListArea;
import io.roboapp.ewf.components.library.LibraryView;
import io.roboapp.ewf.helper.Db;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class MediaResource {

    final private Stage stage;

    final private Db db;

    private final SplitPane splitPane;

    private final LibraryArea libraryArea;

    private final LibraryListArea<MediaItem> libraryListArea;

    private LibraryView libraryView;

    public MediaResource(Application main) {
        this.stage = main.getStage();
        this.db = main.getDb();
        libraryArea = new LibraryArea();
        libraryListArea = new LibraryListArea<>();
        libraryView = new LibraryView();

        splitPane = new SplitPane();
        splitPane.setDividerPositions(0.20, 0.75);
        splitPane.getItems().addAll(buildLibraryArea(), buildLibraryListArea(), buildLibraryViewArea());

        bind();
        loadItem();
    }

    private LibraryArea buildLibraryArea() {

        libraryArea.addCategory(new LibraryArea.Category("Images", new Filter("content", ".+(\\.(?i)(jpeg|jpg|png|gif|bmp))$")));
        libraryArea.addCategory(new LibraryArea.Category("Video", new Filter("content", ".+(\\.(?i)(wmv|ogg|mov|avi|mpg|mkv))$")));
        libraryArea.addCategory(new LibraryArea.Category("Audio", new Filter("content", ".+(\\.(?i)(wma|mp3))$")));
        libraryArea.addCategory(new LibraryArea.Category("Feeds", new Filter()));

        libraryArea.addCollection(new LibraryArea.Collection("My library #1"));
        libraryArea.addCollection(new LibraryArea.Collection("My library #2"));
        libraryArea.addCollection(new LibraryArea.Collection("My library #3"));

        return libraryArea;
    }

    private LibraryListArea<MediaItem> buildLibraryListArea() {

        return libraryListArea;
    }

    private LibraryView buildLibraryViewArea() {

        return libraryView;
    }

    public Tab getTab() {
        Tab tab = new Tab("Media");
        tab.setContent(splitPane);

        return tab;
    }

    private void bind() {

        FilteredList<MediaItem> filteredData = new FilteredList<>(libraryListArea.getMasterData(), p -> true);

        libraryArea.getTreeView().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            libraryListArea.getListView().getSelectionModel().clearSelection();
            Filter filter = newValue.getValue().getFilter();
            filteredData.setPredicate(filter::filtering);
        });

        libraryListArea.setItems(filteredData);
        libraryListArea.getListView().getSelectionModel().selectedItemProperty().addListener(handleOnSelectedListArea);


        libraryListArea.getFloatingActionButton().setOnAction(this::handleOnActionFloatingActionButton);
    }

    private ChangeListener<MediaItem> handleOnSelectedListArea = new ChangeListener<MediaItem>() {
        /**
         * This method needs to be provided by an implementation of
         * {@code ChangeListener}. It is called if the value of an
         * {@link ObservableValue} changes.
         * <p>
         * In general is is considered bad practice to modify the observed value in
         * this method.
         *
         * @param observable The {@code ObservableValue} which value changed
         * @param oldValue   The old value
         * @param newValue
         */
        @Override
        public void changed(ObservableValue<? extends MediaItem> observable, MediaItem oldValue, MediaItem newValue) {
            if (!libraryListArea.getSelectedItems().isEmpty()) {
                System.out.println("changed " + oldValue + "->" + newValue.getContent());
                for (MediaItem item : libraryListArea.getSelectedItems()) {
                    System.out.println(item.getContent());
                    //libraryView.getMediaPlayer().stop();
                    //libraryView.getMediaPlayer().release();

                    libraryView.getMediaPlayer().prepareMedia(item.getContent());
                    libraryView.getMediaPlayer().start();
                }
            }
        }
    };

    private void handleOnActionFloatingActionButton(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Alowed File", "*.*"),
                new FileChooser.ExtensionFilter("Image", "*.jpg"),
                new FileChooser.ExtensionFilter("All File", "*.*")
        );

        List<File> list = fileChooser.showOpenMultipleDialog(stage);
        if (list != null) {
            list.forEach(this::openFile);
        }
    }

    private void openFile(File file) {
        MediaItem item = new MediaItem();
        item.setTitle(file.getName());
        item.setContent(file.getAbsolutePath());
        saveItem(item);
        libraryListArea.add(item);
    }

    private void loadItem() {
        String query = "SELECT id, title, content, background, preview " +
                "FROM library_item " +
                "ORDER BY created_at DESC";
        try {
            ResultSet resultSet = db.executeQry(query);

            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String content = resultSet.getString("content");
                String background = resultSet.getString("background");
                Image preview = null;
                try {
                    preview = new Image(resultSet.getBinaryStream("preview"));
                } catch (Exception ignored) {}

                MediaItem mediaItem = new MediaItem(id, title, content, background, preview);

                libraryListArea.getMasterData().add(mediaItem);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveItem(MediaItem mediaItem) {
        String query = "INSERT INTO library_item(type, title, content, background, created_at) " +
                "VALUES (?, ?, ?, ?, ?)";

        try {
            PreparedStatement pStmt = db.prepareStmt(query);
            pStmt.setString(1, mediaItem.TYPE);
            pStmt.setString(2, mediaItem.getTitle());
            pStmt.setString(3, mediaItem.getContent());
            pStmt.setString(4, mediaItem.getBackground());
            pStmt.setString(5, String.valueOf((new Date()).getTime()));

            pStmt.execute();
            pStmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
