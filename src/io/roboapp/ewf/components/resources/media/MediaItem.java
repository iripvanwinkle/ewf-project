package io.roboapp.ewf.components.resources.media;

import io.roboapp.ewf.Item;
import javafx.scene.image.Image;

public class MediaItem extends Item {

    public final String TYPE = "media";

    public MediaItem(Integer id, String title, String content, String background, Image preview) {
        super(id, title, content, background, preview);
    }

    public MediaItem() {
    }

    public MediaItem(String title) {
        super(title);
    }

    @Override
    public void setContent(String content) {
        this.content = content;

        buildPreview(content);
    }

    protected void buildPreview(String content) {
        setPreview(new Image("file:" + content));
    }

}
