package io.roboapp.ewf.components.resources.songs;

import io.roboapp.ewf.components.library.LibraryArea;
import io.roboapp.ewf.components.library.LibraryListArea;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;

public class SongResource extends Tab {

    public SongResource() {
        super("Songs");

        LibraryArea cv = new LibraryArea();
        LibraryListArea<LibraryListArea.Item> lv = new LibraryListArea<>();


        SplitPane sp = new SplitPane();
        sp.setDividerPositions(0.25, 0.75);
        sp.getItems().addAll(cv, lv, new Pane());

        cv.addCategory(new LibraryArea.Category("Songs"));

        cv.addCollection(new LibraryArea.Collection("My library #1"));
        cv.addCollection(new LibraryArea.Collection("My library #2"));
        cv.addCollection(new LibraryArea.Collection("My library #3"));

        for (int i = 0; i < 20; i++) {
            //lv.add(new LibraryListArea.Item());
        }

        this.setContent(sp);
    }
}
