package io.roboapp.ewf.components.library;

import com.sun.jna.Memory;
import javafx.application.Platform;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.image.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Screen;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

import java.nio.ByteBuffer;

public class LibraryView extends AnchorPane {

    private ImageView imageView;

    private DirectMediaPlayerComponent mediaPlayerComponent;

    private WritableImage writableImage;

    private WritablePixelFormat<ByteBuffer> pixelFormat;

    private FloatProperty videoSourceRatioProperty;

    public LibraryView() {
        mediaPlayerComponent = new CanvasPlayerComponent();
        videoSourceRatioProperty = new SimpleFloatProperty(0.4f);
        pixelFormat = PixelFormat.getByteBgraPreInstance();
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();

        writableImage = new WritableImage((int) visualBounds.getWidth(), (int) visualBounds.getHeight());
        imageView = new ImageView(writableImage);
        imageView.setSmooth(true);

        widthProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("widthProperty");
            System.out.println(getMinWidth());
            fitImageViewSize(newValue.floatValue(), (float) getHeight());
        });

        heightProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("heightProperty");
            fitImageViewSize((float) getWidth(), newValue.floatValue());
        });

        videoSourceRatioProperty.addListener((observable, oldValue, newValue) -> {
            System.out.println("videoSourceRatioProperty");
            fitImageViewSize((float) getWidth(), (float) getHeight());
        });

        fitImageViewSize((float) getWidth(), (float) getHeight());
        getChildren().add(imageView);
        setMinSize(0, 0);

        StackPane stackPane = new StackPane();
        //textFlow.setTextAlignment(TextAlignment.CENTER);
        Text text = new Text("Храню повеления Твои и откровения Твои,\n ибо все пути мои пред Тобою. ");
        text.setTextAlignment(TextAlignment.CENTER);
        text.setX(10.0);
        text.setY(10.0);
        text.setFill(Color.RED);

        stackPane.getChildren().addAll(text);
        AnchorPane.setBottomAnchor(stackPane, 0.0);
        AnchorPane.setLeftAnchor(stackPane, 0.0);
        AnchorPane.setRightAnchor(stackPane, 0.0);
        AnchorPane.setTopAnchor(stackPane, 0.0);


        getChildren().add(stackPane);


    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayerComponent.getMediaPlayer();
    }

    private void fitImageViewSize(float width, float height) {

        //Platform.runLater(() -> {

        float fitHeight = videoSourceRatioProperty.get() * width;
        if (fitHeight > height) {
            imageView.setFitHeight(height);
            double fitWidth = height / videoSourceRatioProperty.get();
            imageView.setFitWidth(fitWidth);
            imageView.setX((width - fitWidth) / 2);
            imageView.setY(0);
        } else {
            imageView.setFitWidth(width);
            imageView.setFitHeight(fitHeight);
            imageView.setY((height - fitHeight) / 2);
            imageView.setX(0);
        }
        //});
    }

    private class CanvasPlayerComponent extends DirectMediaPlayerComponent {

        public CanvasPlayerComponent() {
            super(new CanvasBufferFormatCallback());
        }

        PixelWriter pixelWriter = null;

        private PixelWriter getPW() {
            if (pixelWriter == null) {
                pixelWriter = writableImage.getPixelWriter();
            }
            return pixelWriter;
        }

        @Override
        public void display(DirectMediaPlayer mediaPlayer, Memory[] nativeBuffers, BufferFormat bufferFormat) {
            if (writableImage == null) {
                return;
            }
            Platform.runLater(() -> {
                Memory nativeBuffer = mediaPlayer.lock()[0];
                try {
                    ByteBuffer byteBuffer = nativeBuffer.getByteBuffer(0, nativeBuffer.size());
                    getPW().setPixels(0, 0, bufferFormat.getWidth(), bufferFormat.getHeight(), pixelFormat, byteBuffer, bufferFormat.getPitches()[0]);
                } finally {
                    mediaPlayer.unlock();
                }
            });
        }
    }

    private class CanvasBufferFormatCallback implements BufferFormatCallback {
        @Override
        public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
            Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
            Platform.runLater(() -> videoSourceRatioProperty.set((float) sourceHeight / (float) sourceWidth));
            return new RV32BufferFormat((int) visualBounds.getWidth(), (int) visualBounds.getHeight());
        }
    }
}
