/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.roboapp.ewf.components.library;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.io.IOException;

/**
 * @author robbob
 */
public class LibraryListArea<T extends LibraryListArea.Item> extends VBox {

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private ListView<T> listView;

    @FXML
    private Button floatingActionButton;

    @FXML
    private Button buttonChangeView;

    @FXML
    private Button buttonSearch;

    private ObservableList<T> masterData = FXCollections.observableArrayList();

    private Callback<LibraryListArea, Void> handleOnActionFloatingActionButton;

    private Callback<LibraryListArea, Void> callbackSelectionItem;

    public LibraryListArea() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("ListArea.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void initialize() {
        listView.setCellFactory(param -> new ListCell<T>() {
            @Override
            protected void updateItem(T item, boolean bln) {
                super.updateItem(item, bln);
                setPrefHeight(100);
                if (item != null) {
                    ImageView imageView = new ImageView(item.getPreview());
                    imageView.setSmooth(true);
                    imageView.setPreserveRatio(true);
                    imageView.setFitWidth(100);
                    imageView.setFitWidth(100);
                    setGraphic(imageView);
                    setText(item.getTitle());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });

        listView.setItems(FXCollections.observableArrayList());

    }

    public ListView<T> getListView() {
        return listView;
    }


    public Button getFloatingActionButton() {
        return floatingActionButton;
    }

    public void add(T item) {
        getMasterData().add(item);
    }

    public void setItems(ObservableList<T> value) {
        listView.setItems(value);
    }

    public ObservableList<T> getItems() {
        return listView.getItems();
    }

    public ObservableList<T> getSelectedItems() {
        return listView.getSelectionModel().getSelectedItems();
    }

    public ObservableList<T> getMasterData() {
        return masterData;
    }

    public void setMasterData(ObservableList<T> masterData) {
        this.masterData = masterData;
    }

    public interface Item {

        String getTitle();

        Image getPreview();

        String getLink();

    }
}