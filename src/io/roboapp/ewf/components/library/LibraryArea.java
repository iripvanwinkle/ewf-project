/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.roboapp.ewf.components.library;

import io.roboapp.ewf.Filter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class LibraryArea extends VBox {

    @FXML
    private TreeView<LibraryArea.Item> treeView;

    @FXML
    private Label title;

    private TreeItem<LibraryArea.Item> collections = new TreeItem<>(new Category("Collections", true));

    private TreeItem<LibraryArea.Item> root = new TreeItem<>(new Category("MAIN", true));


    public LibraryArea() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("LibraryArea.fxml"));

        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

    }

    @FXML
    public void initialize() {
        treeView.setCellFactory(tv -> {
            TreeCell<Item> cell = new TreeCell<Item>() {
                @Override
                public void updateItem(Item item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty) {
                        setText("");
                        setGraphic(null);
                    } else {
                        setText(item.getTitle()); // appropriate text for item
                        if (item.existIcon()) {
                            setGraphic(new ImageView(item.getIcon()));
                        }
                    }
                }
            };

            return cell;
        });

        treeView.setShowRoot(false);

        treeView.setRoot(root);

        root.getChildren().add(collections);
    }


    public TreeView<LibraryArea.Item> getTreeView() {
        return treeView;
    }


    public void addCategory(Category category) {
        root.getChildren().add(root.getChildren().size() - 1, new TreeItem<Item>(category));
    }

    public void addCollection(Collection collection) {
        collections.getChildren().add(new TreeItem<>(collection));
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }


    public static abstract class Item {

        final private StringProperty titleProperty;

        final private ObjectProperty<Filter> filterProperty;

        final private ObjectProperty<Image> iconProperty;

        private boolean readOnly = false;

        public Item(String title, boolean readOnly) {
            this.titleProperty = new SimpleStringProperty(title);
            setReadOnly(readOnly);
            this.filterProperty = new SimpleObjectProperty<>(new Filter());
            this.iconProperty = new SimpleObjectProperty<>(null);
        }

        public Item(String title) {
            this.titleProperty = new SimpleStringProperty(title);
            this.filterProperty = new SimpleObjectProperty<>(new Filter());
            this.iconProperty = new SimpleObjectProperty<>(null);
        }

        public Item(String title, Filter filter) {
            this.filterProperty = new SimpleObjectProperty<>(filter);
            this.titleProperty = new SimpleStringProperty(title);
            this.iconProperty = new SimpleObjectProperty<>(null);
        }

        public Item(String title, Filter filter, Image icon) {
            this.filterProperty = new SimpleObjectProperty<>(filter);
            this.titleProperty = new SimpleStringProperty(title);
            this.iconProperty = new SimpleObjectProperty<>(icon);
        }

        public String getTitle() {
            return titleProperty.get();
        }

        public StringProperty titleProperty() {
            return titleProperty;
        }

        public void setTitle(String title) {
            this.titleProperty.set(title);
        }

        public Filter getFilter() {
            return filterProperty().get();
        }

        public ObjectProperty<Filter> filterProperty() {
            return filterProperty;
        }

        public void setFilter(Filter filterProperty) {
            filterProperty().set(filterProperty);
        }

        public ObjectProperty<Image> iconProperty() {
            return iconProperty;
        }

        public Image getIcon() {
            return iconProperty().get();
        }

        public void setIcon(Image icon) {
            iconProperty().set(icon);
        }

        public boolean existIcon() {
            return getIcon() != null;
        }

        public boolean isReadOnly() {
            return readOnly;
        }

        public void setReadOnly(boolean readOnly) {
            this.readOnly = readOnly;
        }
    }

    public static class Category extends Item {

        public Category(String title, boolean readOnly) {
            super(title, readOnly);
        }

        public Category(String title) {
            super(title);
        }

        public Category(String title, Filter filter) {
            super(title, filter);
        }

        public Category(String title, Filter filter, Image icon) {
            super(title, filter, icon);
        }
    }


    public static class Collection extends Item {

        public Collection(String title, boolean readOnly) {
            super(title, readOnly);
        }

        public Collection(String title) {
            super(title);
        }

        public Collection(String title, Filter filter) {
            super(title, filter);
        }

        @Override
        public boolean existIcon() {
            return false;
        }
    }

}