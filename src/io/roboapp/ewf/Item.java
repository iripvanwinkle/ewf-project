package io.roboapp.ewf;


import io.roboapp.ewf.components.library.LibraryListArea;
import javafx.scene.image.Image;

public abstract class Item implements LibraryListArea.Item {

    protected Integer id;
    protected String title;
    protected String content;
    protected String background;
    protected Image preview;

    public Item() {

    }

    public Item(Integer id, String title, String content, String background, Image preview) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.background = background;
        this.preview = preview;
    }

    public Item(String title) {
        setTitle(title);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    @Override
    public Image getPreview() {
        return preview;
    }

    public void setPreview(Image preview) {
        this.preview = preview;
    }

    @Override
    public String getLink() {
        return null;
    }
}
