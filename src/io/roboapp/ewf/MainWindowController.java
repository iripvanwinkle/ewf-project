package io.roboapp.ewf;

import io.roboapp.ewf.i18n.I18N;
import io.roboapp.ewf.menubar.MenuBarController;
import io.roboapp.ewf.util.AbstractWindowController;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainWindowController extends AbstractWindowController {

    private final MenuBarController menuBarController = new MenuBarController(this);

    @FXML
    public VBox menuPanelHost;


    public MainWindowController() {
        super(MainWindowController.class.getResource("MainWindow.fxml"), I18N.getBundle(), false);
        // sizeToScene = false because sizing is defined in preferences

        getStage().setTitle("EWF Project");
    }

    @Override
    protected void controllerDidLoadFxml() {
        assert menuPanelHost != null;

        menuPanelHost.getChildren().add(0, menuBarController.getMenuBar());
    }

    @Override
    protected void onCloseRequest(WindowEvent event) {
        closeWindow();
    }
}
