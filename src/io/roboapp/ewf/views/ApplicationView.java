package io.roboapp.ewf.views;

import io.roboapp.ewf.Application;
import io.roboapp.ewf.components.resources.media.MediaResource;
import io.roboapp.ewf.components.resources.songs.SongResource;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class ApplicationView {

    private final BorderPane borderPane;

    private final Application application;

    public ApplicationView(Application application) {
        this.application = application;

        borderPane = new BorderPane();
        borderPane.setTop(buildTopPanel());
        borderPane.setCenter(buildCenterPanel());
    }

    private VBox buildTopPanel() {
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(new Menu("File"), new Menu("Edit"), new Menu("Help"));

        ToolBar toolBar = new ToolBar();
        toolBar.getItems().addAll(new Button("Button"));

        return new VBox(menuBar, toolBar);
    }

    private SplitPane buildCenterPanel() {
        SplitPane mainSplitPane = new SplitPane();
        mainSplitPane.setDividerPositions(.5);
        mainSplitPane.setOrientation(Orientation.VERTICAL);


        mainSplitPane.getItems().addAll(new AnchorPane(), buildResourcesPane());

        return mainSplitPane;
    }


    private AnchorPane buildResourcesPane() {
        AnchorPane anchorPane = new AnchorPane();
        TabPane resourcesPane = new TabPane();
        resourcesPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        AnchorPane.setLeftAnchor(resourcesPane, 0d);
        AnchorPane.setRightAnchor(resourcesPane, 0d);
        AnchorPane.setBottomAnchor(resourcesPane, 0d);
        AnchorPane.setTopAnchor(resourcesPane, 0d);

        resourcesPane.getTabs().addAll(
                (new MediaResource(application)).getTab(),
                new SongResource()
        );

        anchorPane.getChildren().add(resourcesPane);

        return anchorPane;
    }

    public Parent getParent() {
        return borderPane;
    }

}