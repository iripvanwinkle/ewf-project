package io.roboapp.ewf;

import com.sun.jna.NativeLibrary;

public class Main {

    public static void main(String[] args) {
        NativeLibrary.addSearchPath("libvlc", "C:\\Program Files\\VideoLAN\\VLC");

        javafx.application.Application.launch(Application.class, args);
    }

}
