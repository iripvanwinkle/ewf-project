package io.roboapp.ewf;

import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;

public class Filter {

    final private Map<String, Pattern> filterMap = new Hashtable<>();

    public Filter() {
    }

    public Filter(String ruleKey, String ruleValue) {
        addRule(ruleKey, ruleValue);
    }

    public void addRule(String ruleKey, String ruleValue) {
        filterMap.put(ruleKey, Pattern.compile(ruleValue));
    }

    public boolean filtering(Item item) {
        final Boolean[] result = {true};
        //Class reflect = Item.class;
        filterMap.forEach((key, pattern) -> {
            if (result[0]) {
                //try {
                //Field field = reflect.getField(key);
                Object value = item.getContent(); //field.get(item);
                result[0] = pattern.matcher(value.toString()).matches();
                System.out.println(value.toString() + " " + pattern.toString());
                System.out.println(result[0].toString());

                /*} catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }*/
            }
        });

        return result[0];
    }

}
