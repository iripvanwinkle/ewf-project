package io.roboapp.ewf.menubar;

import io.roboapp.ewf.Application;
import io.roboapp.ewf.MainWindowController;
import io.roboapp.ewf.i18n.I18N;
import io.roboapp.ewf.util.AppPlatform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.StackPane;

import java.io.IOException;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class MenuBarController {
    private static MenuBarController systemMenuBarController; // For Mac only

    private final MainWindowController mainWindowController;

    @FXML
    public MenuBar menuBar;

    @FXML
    public Menu fileMenu;
    @FXML
    public Menu liveMenu;
    @FXML
    public Menu helpMenu;
    // File Menu
    @FXML
    public MenuItem newMenuItem;
    @FXML
    public MenuItem openScheduleMenuItem;
    @FXML
    public MenuItem openRecentMenuItem;
    @FXML
    public MenuItem saveScheduleMenuItem;
    @FXML
    public MenuItem saveScheduleAsMenuItem;
    @FXML
    public MenuItem closeScheduleMenuItem;
    @FXML
    public MenuItem preferenceMenuItem;
    @FXML
    public MenuItem exitMenuItem;

    // Live Menu
    @FXML
    public MenuItem goLiveMenuItem;
    @FXML
    public MenuItem logoOnLiveMenuItem;
    @FXML
    public MenuItem blackOnLiveMenuItem;
    @FXML
    public MenuItem clearTextOnLiveMenuItem;
    @FXML
    public MenuItem showLiveMenuItem;

    // Help Menu
    @FXML
    public MenuItem projectHelpMenuItem;
    @FXML
    public MenuItem aboutMenuItem;

    private static final KeyCombination.Modifier modifier;
    private final Map<KeyCombination, MenuItem> keyToMenu = new HashMap<>();

    static {
        if (AppPlatform.IS_MAC) {
            modifier = KeyCombination.META_DOWN;
        } else {
            // Should cover Windows, Linux
            modifier = KeyCombination.CONTROL_DOWN;
        }
    }

    public MenuBarController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    public MenuBar getMenuBar() {

        if (menuBar == null) {
            final URL fxmlURL = MenuBarController.class.getResource("MenuBar.fxml"); //NOI18N
            final FXMLLoader loader = new FXMLLoader();

            loader.setController(this);
            loader.setLocation(fxmlURL);
            loader.setResources(I18N.getBundle());
            try {
                loader.load();
                controllerDidLoadFxml();
            } catch (RuntimeException | IOException x) {
                System.out.println("loader.getController()=" + loader.getController()); //NOI18N
                System.out.println("loader.getLocation()=" + loader.getLocation()); //NOI18N
                throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
            }
        }

        return menuBar;
    }

    private void controllerDidLoadFxml() {
        /*
         * On Mac, move the menu bar on the desktop and remove the Quit item
         * from the File menu
         */
        if (AppPlatform.IS_MAC) {
            menuBar.setUseSystemMenuBar(true);
            exitMenuItem.getParentMenu().getItems().remove(exitMenuItem);
        }

        /*
         * File menu
         */
        //newMenuItem.setUserData(new ApplicationControlActionController(ApplicationControlAction.NEW_FILE));
        newMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.N, modifier));

        for (Menu m : menuBar.getMenus()) {
            setupMenuItemHandlers(m);
        }
    }

    /*
    * Generic menu and item handlers
    */
    private void setupMenuItemHandlers(MenuItem i) {
        if (i instanceof Menu) {
            final Menu m = (Menu) i;
            m.setOnMenuValidation(onMenuValidationEventHandler);
            for (MenuItem child : m.getItems()) {
                setupMenuItemHandlers(child);
            }
        } else {
            i.setOnAction(onActionEventHandler);
            if (i.getAccelerator() != null) {
                keyToMenu.put(i.getAccelerator(), i);
            }
        }
    }

    private final EventHandler<Event> onMenuValidationEventHandler
            = t -> {
        handleOnMenuValidation((Menu) t.getSource());
    };

    private void handleOnMenuValidation(Menu menu) {
        for (MenuItem i : menu.getItems()) {
            final boolean disable, selected;
            final String title;
            if (i.getUserData() instanceof MenuItemController) {
                final MenuItemController c = (MenuItemController) i.getUserData();
                boolean canPerform;
                try {
                    canPerform = c.canPerform();
                } catch (RuntimeException x) {
                    // This catch is protection against a bug in canPerform().
                    // It avoids to block all the items in the menu in case
                    // of crash in canPerform() (see DTL-6164).
                    canPerform = false;
                    final Exception xx
                            = new Exception(c.getClass().getSimpleName()
                            + ".canPerform() did break for menu item " + i, x); //NOI18N
                    xx.printStackTrace();
                }
                disable = !canPerform;
                title = c.getTitle();
                selected = c.isSelected();
            } else {
                if (i instanceof Menu) {
                    disable = false;
                    selected = false;
                    title = null;
                } else {
                    disable = true;
                    selected = false;
                    title = null;
                }
            }
            i.setDisable(disable);
            if (title != null) {
                i.setText(title);
            }
            if (i instanceof RadioMenuItem) {
                final RadioMenuItem ri = (RadioMenuItem) i;
                ri.setSelected(selected);
            }
        }
    }

    private final EventHandler<ActionEvent> onActionEventHandler
            = t -> {
        assert t.getSource() instanceof MenuItem;
        handleOnActionMenu((MenuItem) t.getSource());
    };

    private void handleOnActionMenu(MenuItem i) {
        assert i.getUserData() instanceof MenuItemController;
        final MenuItemController c = (MenuItemController) i.getUserData();
        c.perform();
    }

    /*
     * Private (MenuItemController)
     */
    abstract class MenuItemController {

        public abstract boolean canPerform();

        public abstract void perform();

        public String getTitle() {
            return null;
        }

        public boolean isSelected() {
            return false;
        }
    }
}
