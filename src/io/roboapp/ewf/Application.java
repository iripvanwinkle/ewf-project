package io.roboapp.ewf;

import io.roboapp.ewf.helper.Db;

import java.io.IOException;

import io.roboapp.ewf.menubar.MenuBarController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Application extends javafx.application.Application {

    private Stage stage;

    private Db db = null;

    private BorderPane root;

    //private final MenuBarController menuBarController = new MenuBarController(this);

    @FXML
    public VBox menuPanelHost;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        MainWindowController mainWindowController = new MainWindowController();
        mainWindowController.openWindow();

        primaryStage.setOnCloseRequest(event -> {
            mainWindowController.closeWindow();
            Platform.exit();
            System.exit(0);
        });
    }

    protected void controllerDidLoadFxml() {
        //menuPanelHost.getChildren().add(0, menuBarController.getMenuBar());
    }

    public Stage getStage() {
        return stage;
    }

    public Db getDb() {
        if (db == null) {
            String sDriverForClass = "org.sqlite.JDBC";
            String sUrlString = "jdbc:sqlite:data.sqlite";
            try {
                db = new Db(sDriverForClass, sUrlString);
            } catch (Exception e) {
                System.out.println("DB: Connection failed");
                System.out.println(e.toString());
            }
        }

        return db;
    }
}
