package io.roboapp.ewf.util;


import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public abstract class AbstractWindowController {
    private Window owner;
    private Parent root;
    private Scene scene;
    private Stage stage;

    private URL fxmlURL;
    private ResourceBundle resources;

    private boolean sizeToScene; // true by default

    private final EventHandler<WindowEvent> closeRequestHandler = event -> {
        onCloseRequest(event);
        event.consume();
    };

    protected abstract void onCloseRequest(WindowEvent event);

    public AbstractWindowController() {
        this(null, true);
    }

    public AbstractWindowController(Window owner) {
        this(owner, true);
    }

    public AbstractWindowController(Window owner, boolean sizeToScene) {
        this.owner = owner;
        this.sizeToScene = sizeToScene;
    }

    public AbstractWindowController(URL fxmlURL, ResourceBundle resources, boolean sizeToScene) {
        this(fxmlURL, resources, null, sizeToScene);
    }

    public AbstractWindowController(URL fxmlURL, ResourceBundle resources, Window owner, boolean sizeToScene) {
        this(owner, sizeToScene);
        assert fxmlURL != null : "Check fxml path given to " + getClass().getSimpleName();
        this.fxmlURL = fxmlURL;
        this.resources = resources;
    }

    public URL getFXMLURL() {
        return fxmlURL;
    }

    public ResourceBundle getResources() {
        return resources;
    }

    public Parent getRoot() {
        if (root == null) {
            makeRoot();
            assert root != null;
            //toolStylesheetDidChange(null);
        }

        return root;
    }

    public Scene getScene() {
        assert Platform.isFxApplicationThread();

        if (scene == null) {
            scene = new Scene(getRoot());
            controllerDidCreateScene();
        }

        return scene;
    }

    public Stage getStage() {
        assert Platform.isFxApplicationThread();

        if (stage == null) {
            stage = new Stage();
            stage.initOwner(owner);
            stage.setOnCloseRequest(closeRequestHandler);
            stage.setScene(getScene());

            if (sizeToScene) {
                stage.sizeToScene();
            }
            controllerDidCreateStage();
        }

        return stage;
    }

    protected final void setRoot(Parent root) {
        assert root != null;

        this.root = root;
    }

    /**
     * Opens this window and place it in front.
     */
    public void openWindow() {
        assert Platform.isFxApplicationThread();

        getStage().show();
        getStage().toFront();
    }

    /**
     * Closes this window.
     */
    public void closeWindow() {
        assert Platform.isFxApplicationThread();
        getStage().close();
    }


    protected void controllerDidCreateScene() {
        assert getRoot() != null;
        assert getRoot().getScene() != null;
        assert getRoot().getScene().getWindow() == null;
    }

    protected void controllerDidCreateStage() {
        assert getRoot() != null;
        assert getRoot().getScene() != null;
        assert getRoot().getScene().getWindow() != null;
    }

    protected void controllerDidLoadFxml() {
        assert getRoot() != null;
        assert getRoot().getScene() == null;
    }

    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();

        loader.setController(this);
        loader.setLocation(fxmlURL);
        //loader.setResources(resources);
        try {
            setRoot((Region) loader.load());
            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }

}
