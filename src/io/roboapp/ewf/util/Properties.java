package io.roboapp.ewf.util;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Properties extends java.util.Properties {

    private Connection connection = null;

    private String linkKey = "global";

    public void connect(Connection connection) {
        this.connection = connection;
    }

    public Properties() {
        super(null);
    }

    public Properties(java.util.Properties defaults) {
        super(defaults);
    }

    public void link(String key) {
        this.linkKey = key;
    }

    public synchronized void load(Reader reader) throws IOException {
        if (connection == null) {
            super.load(reader);
        }
    }

    @Override
    public synchronized void load(InputStream inStream) throws IOException {
        if (connection == null) {
            super.load(inStream);
        }
    }

    @Override
    public String getProperty(String key) {
        Object oVal = get(key);
        String sVal = (oVal instanceof String) ? (String) oVal : getPropertyFromDb(key);
        return ((sVal == null) && (defaults != null)) ? defaults.getProperty(key) : sVal;
    }

    public void store() {
        Hashtable<String, String> h = new Hashtable<>();
        try {
            indexed(h);
            PreparedStatement pStmt = connection.prepareStatement(QUERY_STORE);
            for (Map.Entry<String, String> entry : h.entrySet()) {
                String itemId = String.valueOf(linkKey);
                String propertyId = entry.getKey();
                String value = String.valueOf(get(entry.getValue()));

                pStmt.setString(1, itemId);
                pStmt.setString(2, propertyId);
                pStmt.setString(3, value);
                pStmt.execute();
            }

            pStmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void indexed(Hashtable<String, String> h) throws SQLException {
        StringBuilder names = new StringBuilder("''");
        keySet().forEach(v -> names.append(",'").append(v).append("'"));
        PreparedStatement pStmt = connection.prepareStatement(QUERY_GET_ID.replace("?", names));

        ResultSet resultSet = pStmt.executeQuery();
        while (resultSet.next()) {
            h.put(resultSet.getString("id"), resultSet.getString("name"));
        }
        resultSet.close();
        pStmt.close();
    }

    private String getPropertyFromDb(String key) {
        String value = null;
        try {
            PreparedStatement pStmt = connection.prepareStatement(QUERY_GET_VALUE);
            pStmt.setString(1, key);
            pStmt.setString(2, String.valueOf(linkKey));
            ResultSet resultSet = pStmt.executeQuery();
            if (resultSet.next()) {
                value = resultSet.getString(1);
            }
            resultSet.close();
            pStmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return value;
    }

    final static String QUERY_GET_VALUE =
            "SELECT value " +
                    "FROM properties " +
                    "JOIN property ON id = propertyId " +
                    "WHERE name=? AND key=? ";

    final static String QUERY_GET_ID =
            "SELECT id, name " +
                    "FROM property " +
                    "WHERE name IN (?)";

    final static String QUERY_STORE =
            "INSERT OR REPLACE INTO properties(key, propertyId, value) " +
                    "VALUES (?, ?, ?)";
}
